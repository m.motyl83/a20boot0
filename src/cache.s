

.syntax unified
.global enable_icache
.global disable_icache
.global enable_dcache
.global disable_dcache
.global flush_dcache
.global flush_icache

.arm


.section .text



enable_icache:
            mov r0, #0
            mcr p15, 0, r0, c7, c5, 0
                mrc p15, 0, r0, c1, c0
                orr r0, r0, #(1 << 12)
                mcr p15, 0, r0, c1, c0
                mov pc, lr



enable_dcache:
    mov r0, #0
    mcr p15, 0, r0, c7, c5, 0
        mrc p15, 0, r0, c1, c0
        orr r0, r0, #(1 << 12)
        mcr p15, 0, r0, c1, c0
        mov pc, lr



disable_icache:
        mov r0, #0
    mcr p15, 0, r0, c7, c5, 0
        mrc p15, 0, r0, c1, c0
        bic r0, r0, #(1 << 2)
        mcr p15, 0, r0, c1, c0
        mov pc, lr


disable_dcache:
        mov r0, #0
    mcr p15, 0, r0, c7, c5, 0
        mrc p15, 0, r0, c1, c0
        bic r0, r0, #(1 << 2)
        mcr p15, 0, r0, c1, c0
        mov pc, lr



flush_dcache:
    MRC     p15, 1, r0, c0, c0, 1
    ANDS    r3, r0, #0x7000000
    MOV     r3, r3, lsr #23
    BEQ     finished

    mov     r10, #0
loop1:
    ADD     r2, r10, r10, lsr #1
    MOV     r1, r0, lsr r2
    AND     r1, r1, #7
    CMP     r1, #2
    BLT     skip
    MCR     p15, 2, r10, c0, c0, 0
    ISB
    MRC     p15, 1, r1, c0, c0, 0
    AND     r2, r1, #7
    ADD     r2, r2, #4
    LDR     r4, =0x3ff
    ANDS    r4, r4, r1, lsr #3
    CLZ     r5, r4
    LDR     r7, =0x7fff
    ANDS    r7, r7, r1, lsr #13
loop2:
    MOV     r9, r4
loop3:
    ORR     r11, r10, r9, lsl r5
    ORR     r11, r11, r7, lsl r2
    MCR     p15, 0, r11, c7, c6, 2
    SUBS    r9, r9, #1
    BGE     loop3
    SUBS    r7, r7, #1
    BGE     loop2
skip:
    ADD     r10, r10, #2
    CMP     r3, r10
    BGT     loop1
finished:
    MOV     r10, #0
    MCR     p15, 2, r10, c0, c0, 0
    ISB
    MOV     pc, lr



flush_icache:
    MOV     r0, #0
    MCR     p15, 0, r0, c7, c5, 0
    MOV     pc, lr




enable_prediction:
        stmfd   sp!, {r0, lr}
    bl 		rd_label0
rd_label0:
        bl 		rd_label1
rd_label1:
        bl 		rd_label2
rd_label2:
        bl 		rd_label3
rd_label3:
        bl 		rd_label4
rd_label4:
        bl 		rd_label5
rd_label5:
        bl 		rd_label6
rd_label6:
        bl 		rd_label7
rd_label7:

    mrc     p15, 0, r0, c1, c0, 0
    orr     r0, r0, #(0x1 <<11 )
    mcr     p15, 0, r0, c1, c0, 0

    ldmfd   sp!, {r0, lr}
    mov     pc, lr



.end


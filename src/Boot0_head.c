#include "boot0_i.h"


#define   DDR3_USED

const boot0_file_head_t  BT0_head __attribute__ ((section (".header"))) = {
                                              {
                  /* jump_instruction */          ( 0xEA000000 | ( ( ( sizeof( boot0_file_head_t ) + sizeof( int ) - 1 ) / sizeof( int ) - 2 ) & 0x00FFFFFF ) ),
                                                  BOOT0_MAGIC,
                                                  STAMP_VALUE,
                                                  0x2000,
                                                  sizeof( boot_file_head_t ),
                                                  BOOT_PUB_HEAD_VERSION,
                                                  BOOT0_FILE_HEAD_VERSION,
                                                  BOOT0_VERSION,
                                                  EGON_VERSION,
                                                  {
                                                    0, 0, '2','.','0','.','0',0
                                                  },
                                              },
        #ifdef  DDR3_USED
                                              {
                                                  sizeof( boot0_private_head_t ),
                                                  BOOT0_PRVT_HEAD_VERSION,
                                                  { 0 },
                                                  0,
                                                  {
                                                    { 2, 22, 4, 1, 1, 0, 0, 0},
                                                    { 2, 23, 4, 1, 1, 0, 0, 0}
                                                  },
                                                  0,
                                                  { 0 },
                                                  { 0 },
                                                  { 0 }
                                              }
        #else
                                              {
                                                  sizeof( boot0_private_head_t ),
                                                  BOOT0_PRVT_HEAD_VERSION,
                                                  { 0x40000000,
                                                    1024,
                                                    180,
                                                    1,
                                                    1,
                                                    0,
                                                    (__dram_type_e)1,
                                                    16,
                                                    10,
                                                    14,
                                                    4,
                                                    3,
                                                    0,
                                                    16,
                                                    1024
                                                  },
                                                  0,
                                                  {
                                                    { 2, 22, 4, 1, 1, 0, 0, 0},
                                                    { 2, 23, 4, 1, 1, 0, 0, 0}
                                                  },
                                                  0,
                                                  { 0 },
                                                  { 0 },
                                                  { 0 }
                                              }
        #endif
                                          };

.syntax unified
.global mmu_set_ttb
.global mmu_set_domain_access
.global mmu_flush_TLB
.global mmu_set_smp
.arm


.section .text

mmu_set_ttb:
        MCR p15, 0, r0, c2, c0, 0
        MOV pc, lr



mmu_set_domain_access:
        MRC     p15, 0, r0, c3, c0, 0
        LDR     r0, =0x55555555
        MCR     p15, 0, r0, c3, c0, 0
        MOV     pc, lr



mmu_flush_TLB:
    MOV r0, #0
    MCR p15, 0, r0, c8, c7, 0
    MOV pc, lr


mmu_set_smp:
    mrc  p15, 0, r0, c1, c0, 1
    orr  r0, r0, #(1 << 6)
    mcr  p15, 0, r0, c1, c0, 1

.end

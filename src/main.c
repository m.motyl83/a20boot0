#include "arm_a8.h"
#include "boot0_i.h"
#include "func_in_asm.h"

#include <string.h>

#define SUNXI_UART0_BASE	0x01C28000
#define SUNXI_PIO_BASE		0x01C20800
#define AW_CCM_BASE		0x01c20000
#define AW_SRAMCTRL_BASE	0x01c00000

/*****************************************************************************
 * GPIO code, borrowed from U-Boot                                           *
 *****************************************************************************/

#define SUNXI_GPIO_A    0
#define SUNXI_GPIO_B    1
#define SUNXI_GPIO_C    2
#define SUNXI_GPIO_D    3
#define SUNXI_GPIO_E    4
#define SUNXI_GPIO_F    5
#define SUNXI_GPIO_G    6
#define SUNXI_GPIO_H    7
#define SUNXI_GPIO_I    8

int sunxi_gpio_output(u32 pin, u32 val);
int gpio_direction_output(unsigned gpio, int value);

struct sunxi_gpio {
    u32 cfg[4];
    u32 dat;
    u32 drv[2];
    u32 pull[2];
};

struct sunxi_gpio_reg {
    struct sunxi_gpio gpio_bank[10];
};

#define GPIO_BANK(pin)		((pin) >> 5)
#define GPIO_NUM(pin)		((pin) & 0x1F)

#define GPIO_CFG_INDEX(pin)	(((pin) & 0x1F) >> 3)
#define GPIO_CFG_OFFSET(pin)	((((pin) & 0x1F) & 0x7) << 2)

#define GPIO_PULL_INDEX(pin)	(((pin) & 0x1f) >> 4)
#define GPIO_PULL_OFFSET(pin)	((((pin) & 0x1f) & 0xf) << 1)

/* GPIO bank sizes */
#define SUNXI_GPIO_A_NR    (32)
#define SUNXI_GPIO_B_NR    (32)
#define SUNXI_GPIO_C_NR    (32)
#define SUNXI_GPIO_D_NR    (32)
#define SUNXI_GPIO_E_NR    (32)
#define SUNXI_GPIO_F_NR    (32)
#define SUNXI_GPIO_G_NR    (32)
#define SUNXI_GPIO_H_NR    (32)
#define SUNXI_GPIO_I_NR    (32)

#define SUNXI_GPIO_NEXT(__gpio) ((__gpio##_START) + (__gpio##_NR) + 0)

enum sunxi_gpio_number {
    SUNXI_GPIO_A_START = 0,
    SUNXI_GPIO_B_START = SUNXI_GPIO_NEXT(SUNXI_GPIO_A),
    SUNXI_GPIO_C_START = SUNXI_GPIO_NEXT(SUNXI_GPIO_B),
    SUNXI_GPIO_D_START = SUNXI_GPIO_NEXT(SUNXI_GPIO_C),
    SUNXI_GPIO_E_START = SUNXI_GPIO_NEXT(SUNXI_GPIO_D),
    SUNXI_GPIO_F_START = SUNXI_GPIO_NEXT(SUNXI_GPIO_E),
    SUNXI_GPIO_G_START = SUNXI_GPIO_NEXT(SUNXI_GPIO_F),
    SUNXI_GPIO_H_START = SUNXI_GPIO_NEXT(SUNXI_GPIO_G),
    SUNXI_GPIO_I_START = SUNXI_GPIO_NEXT(SUNXI_GPIO_H),
};

/* SUNXI GPIO number definitions */
#define SUNXI_GPA(_nr)          (SUNXI_GPIO_A_START + (_nr))
#define SUNXI_GPB(_nr)          (SUNXI_GPIO_B_START + (_nr))
#define SUNXI_GPC(_nr)          (SUNXI_GPIO_C_START + (_nr))
#define SUNXI_GPD(_nr)          (SUNXI_GPIO_D_START + (_nr))
#define SUNXI_GPE(_nr)          (SUNXI_GPIO_E_START + (_nr))
#define SUNXI_GPF(_nr)          (SUNXI_GPIO_F_START + (_nr))
#define SUNXI_GPG(_nr)          (SUNXI_GPIO_G_START + (_nr))
#define SUNXI_GPH(_nr)          (SUNXI_GPIO_H_START + (_nr))
#define SUNXI_GPI(_nr)          (SUNXI_GPIO_I_START + (_nr))

/* GPIO pin function config */
#define SUNXI_GPIO_INPUT        (0)
#define SUNXI_GPIO_OUTPUT       (1)
#define SUN4I_GPB_UART0         (2)
#define SUN5I_GPB_UART0         (2)
#define SUN6I_GPH_UART0         (2)
#define SUN8I_H3_GPA_UART0      (2)
#define SUN8I_V3S_GPB_UART0	(3)
#define SUN50I_H5_GPA_UART0     (2)
#define SUN50I_A64_GPB_UART0    (4)
#define SUNXI_GPF_UART0         (4)

/* GPIO pin pull-up/down config */
#define SUNXI_GPIO_PULL_DISABLE (0)
#define SUNXI_GPIO_PULL_UP      (1)
#define SUNXI_GPIO_PULL_DOWN    (2)


#define set_wbit(addr, v)	(*((volatile unsigned long  *)(addr)) |= (unsigned long)(v))
#define readl(addr)		(*((volatile unsigned long  *)(addr)))
#define writel(v, addr)		(*((volatile unsigned long  *)(addr)) = (unsigned long)(v))
//#if 1

int sunxi_gpio_set_cfgpin(u32 pin, u32 val);
extern const boot0_file_head_t BT0_head;
extern __u32 super_standby_flag;
extern void cpuX_startup_to_wfi(void);
void soc_detection_init(void);
static __s32 check_bootid(void);
static void timer_init(void);
static void print_version(void);
void close_cpuX(__u32 cpu);
void open_cpuX(__u32 cpu);
void gpio_init(void);
void uart0_init(void);
void uart0_puts(const char *s);
static __u32 cpu_freq = 0;
static __u32 overhead = 0;


#define CCU_REG_VA (0xF1c20000)
#define CCU_REG_PA (0x01c20000)

int main() {
  __u32 status;
  __s32 dram_size;


  gpio_direction_output(SUNXI_GPH(2),1);
  timer_init();

  UART_open(BT0_head.prvt_head.uart_port, (void *)BT0_head.prvt_head.uart_ctrl,
            24 * 1000 * 1000);



//  if (BT0_head.prvt_head.enable_jtag) {
//    jtag_init((normal_gpio_cfg *)BT0_head.prvt_head.jtag_gpio);
//  }


  msg("HELLO! BOOT0 is starting!\n");

  //jtagEnable();


  print_version();

  init_perfcounters(1, 0);
  change_runtime_env(0);

  open_cpuX(1);
  close_cpuX(1);

  mmu_system_init(EGON2_DRAM_BASE, 1 * 1024, EGON2_MMU_BASE);
  mmu_enable();

  if (BT0_head.boot_head.platform[7]) {
    msg("read dram para.\n");
  } else {
    msg("try dram para.\n");
  }

  dram_size = init_DRAM(BT0_head.boot_head.platform[7]);

  if (dram_size) {
    msg("dram size =%dMB\n", dram_size);
  } else {
    msg("initializing SDRAM Fail.\n");
    mmu_disable();
    jump_to(FEL_BASE);
  }

  // msg("%x\n", *(volatile int *)0x52000000);
  // msg("super_standby_flag = %d\n", super_standby_flag);

  if (1 == super_standby_flag) {
    // tmr_enable_watchdog();
    // disable_icache();
    jump_to(0x52000000);
  }

  status = load_Boot1_from_nand();

  mmu_disable();

  if (status == OK) {
    // restart_watch_dog( );
    // set_dram_size(dram_size );

    set_dram_para((void *)&BT0_head.prvt_head.dram_para, dram_size);

    set_nand_good_block_ratio_para((void *)&BT0_head.prvt_head.storage_data);

    msg("Succeed in loading Boot1.\n"
        "Jump to Boot1.\n");

    jump_to(BOOT1_BASE);
  } else {
    //		disable_watch_dog( );                     // disable watch dog

    msg("Fail in loading Boot1.\n"
        "Jump to Fel.\n");
    jump_to(FEL_BASE);
  }

  while (1)
    ;
}

void set_pll(void) {
  __u32 reg_val, i;

  // use 24MHz crystal
  CCMU_REG_AHB_APB = 0x00010010;

  // set PLL1 384MHz
  reg_val = (0x21005000) | (0x80000000);
  CCMU_REG_PLL1_CTRL = reg_val;

  // wait for pll
  for (i = 0; i < 200; i++)
    ;

  //切换到PLL1
  reg_val = CCMU_REG_AHB_APB;
  reg_val &= ~(3 << 16);
  reg_val |= (2 << 16);
  CCMU_REG_AHB_APB = reg_val;

  // enable DMA
  CCMU_REG_AHB_MOD0 |= 1 << 6;

  // set PLL6 at 600MHz
  reg_val = CCMU_REG_PLL6_CTRL;
  reg_val |= 1 << 31;
  CCMU_REG_PLL6_CTRL = reg_val;

  return;
}

static __s32 check_bootid(void) {
#if 0
    __u32 reg = 0x01c23800;
    __u32 value, i;

    for(i=0;i<4;i++)
    {
        value = *(volatile __u32 *)(reg + 0x10 + (i<<2));
        if(value)
        {
            return -1;
        }
    }
#endif
  return 0;
}

static void timer_init(void) {
  *(volatile unsigned int *)(0x01c20000 + 0x144) |= (1U << 31);
  *(volatile unsigned int *)(0x01c20C00 + 0x80) = 1;
  *(volatile unsigned int *)(0x01c20C00 + 0x8C) = 0x2EE0;
  *(volatile unsigned int *)(0x01c20C00 + 0x84) = 0;
}

static void print_version(void) {
  msg("boot0 version : %s\n", BT0_head.boot_head.platform + 2);
}

__u32 get_cyclecount(void) {
  __u32 value;
  // Read CCNT Register

  __asm volatile("MRC p15, 0, %0, c9, c13, 0 " : "=r"(value));

  return value;
}

void init_perfcounters(__u32 do_reset, __u32 enable_divider) {
  // in general enable all counters (including cycle counter)
  __u32 value = 1;

  // peform reset:
  if (do_reset) {
    value |= 2; // reset all counters to zero.
    value |= 4; // reset cycle counter to zero.
  }

  if (enable_divider)
    value |= 8; // enable "by 64" divider for CCNT.

  value |= 16;

  // program the performance-counter control-register:
  __asm volatile("MCR p15, 0, %0, c9, c12, 0" : : "r"(value));

  // enable all counters:
  value = 0x8000000f;
  __asm volatile("MCR p15, 0, %0, c9, c12, 1" : : "r"(value));

  // clear overflows:
  __asm volatile("MCR p15, 0, %0, c9, c12, 3" : : "r"(value));

  return;
}

void change_runtime_env(__u32 mmu_flag) {
  __u32 factor_n = 0;
  __u32 factor_k = 0;
  __u32 factor_m = 0;
  __u32 factor_p = 0;
  __u32 start = 0;
  __u32 cmu_reg = 0;
  volatile __u32 reg_val = 0;

  if (mmu_flag) {
    cmu_reg = CCU_REG_VA;
  } else {
    cmu_reg = CCU_REG_PA;
  }
  // init counters:
  // init_perfcounters (1, 0);
  // measure the counting overhead:
  start = get_cyclecount();
  overhead = get_cyclecount() - start;
  // busy_waiting();
  // get runtime freq: clk src + divider ratio
  // src selection
  reg_val = *(volatile int *)(cmu_reg + 0x54);
  reg_val >>= 16;
  reg_val &= 0x3;
  if (0 == reg_val) {
    // 32khz osc
    cpu_freq = 32;

  } else if (1 == reg_val) {
    // hosc, 24Mhz
    cpu_freq = 24000; // unit is khz
  } else if (2 == reg_val) {
    // get pll_factor
    reg_val = *(volatile int *)(cmu_reg + 0x00);
    factor_p = 0x3 & (reg_val >> 16);
    factor_p = 1 << factor_p;              // 1/2/4/8
    factor_n = 0x1f & (reg_val >> 8);      // the range is 0-31
    factor_k = (0x3 & (reg_val >> 4)) + 1; // the range is 1-4
    factor_m = (0x3 & (reg_val >> 0)) + 1; // the range is 1-4

    cpu_freq = (24000 * factor_n * factor_k) / (factor_p * factor_m);
    // cpu_freq = raw_lib_udiv(24000*factor_n*factor_k, factor_p*factor_m);
    // msg("cpu_freq = dec(%d). \n", cpu_freq);
    // busy_waiting();
  }
}

/*
 * input para range: 1-1000 us, so the max us_cnt equal = 1008*1000;
 */
void delay_us(__u32 us) {
  __u32 us_cnt = 0;
  __u32 cur = 0;
  __u32 target = 0;

  // us_cnt = ((raw_lib_udiv(cpu_freq, 1000)) + 1)*us;
  us_cnt = ((cpu_freq / 1000) + 1) * us;
  cur = get_cyclecount();
  target = cur - overhead + us_cnt;

#if 1
  while (!counter_after_eq(cur, target)) {
    cur = get_cyclecount();
    // cnt++;
  }
#endif

  return;
}

#define SW_PA_CPUCFG_IO_BASE 0x01c25c00
/*
 * CPUCFG
 */
#define AW_CPUCFG_P_REG0 0x01a4
#define CPUX_RESET_CTL(x) (0x40 + (x)*0x40)
#define CPUX_CONTROL(x) (0x44 + (x)*0x40)
#define CPUX_STATUS(x) (0x48 + (x)*0x40)
#define AW_CPUCFG_GENCTL 0x0184
#define AW_CPUCFG_DBGCTL0 0x01e0
#define AW_CPUCFG_DBGCTL1 0x01e4

#define AW_CPU1_PWR_CLAMP 0x01b0
#define AW_CPU1_PWROFF_REG 0x01b4
#define readl(addr) (*((volatile unsigned long *)(addr)))
#define writel(v, addr)                                                        \
  (*((volatile unsigned long *)(addr)) = (unsigned long)(v))

#define IO_ADDRESS(IO_ADDR) (IO_ADDR)
#define IS_WFI_MODE(cpu)                                                       \
  (readl(IO_ADDRESS(SW_PA_CPUCFG_IO_BASE) + CPUX_STATUS(cpu)) & (1 << 2))

void open_cpuX(__u32 cpu) {
  long paddr;
  __u32 pwr_reg;

  paddr = (__u32)cpuX_startup_to_wfi;
  writel(paddr, IO_ADDRESS(SW_PA_CPUCFG_IO_BASE) + AW_CPUCFG_P_REG0);

  /* step1: Assert nCOREPORESET LOW and hold L1RSTDISABLE LOW.
            Ensure DBGPWRDUP is held LOW to prevent any external
            debug access to the processor.
  */
  /* assert cpu core reset */
  writel(0, IO_ADDRESS(SW_PA_CPUCFG_IO_BASE) + CPUX_RESET_CTL(cpu));
  /* L1RSTDISABLE hold low */
  pwr_reg = readl(IO_ADDRESS(SW_PA_CPUCFG_IO_BASE) + AW_CPUCFG_GENCTL);
  pwr_reg &= ~(1 << cpu);
  writel(pwr_reg, IO_ADDRESS(SW_PA_CPUCFG_IO_BASE) + AW_CPUCFG_GENCTL);
  /* DBGPWRDUP hold low */
  pwr_reg = readl(IO_ADDRESS(SW_PA_CPUCFG_IO_BASE) + AW_CPUCFG_DBGCTL1);
  pwr_reg &= ~(1 << cpu);
  writel(pwr_reg, IO_ADDRESS(SW_PA_CPUCFG_IO_BASE) + AW_CPUCFG_DBGCTL1);

  /* step3: clear power-off gating */
  pwr_reg = readl(IO_ADDRESS(SW_PA_CPUCFG_IO_BASE) + AW_CPU1_PWROFF_REG);
  pwr_reg &= ~(1);
  writel(pwr_reg, IO_ADDRESS(SW_PA_CPUCFG_IO_BASE) + AW_CPU1_PWROFF_REG);
  delay_us(1000);

  /* step4: de-assert core reset */
  writel(3, IO_ADDRESS(SW_PA_CPUCFG_IO_BASE) + CPUX_RESET_CTL(cpu));

  /* step5: assert DBGPWRDUP signal */
  pwr_reg = readl(IO_ADDRESS(SW_PA_CPUCFG_IO_BASE) + AW_CPUCFG_DBGCTL1);
  pwr_reg |= (1 << cpu);
  writel(pwr_reg, IO_ADDRESS(SW_PA_CPUCFG_IO_BASE) + AW_CPUCFG_DBGCTL1);
}

void close_cpuX(__u32 cpu) {
  __u32 pwr_reg;

  while (!IS_WFI_MODE(cpu))
    ;
  /* step1: deassert cpu core reset */
  writel(0, IO_ADDRESS(SW_PA_CPUCFG_IO_BASE) + CPUX_RESET_CTL(cpu));

  /* step2: deassert DBGPWRDUP signal */
  pwr_reg = readl(IO_ADDRESS(SW_PA_CPUCFG_IO_BASE) + AW_CPUCFG_DBGCTL1);
  pwr_reg &= ~(1 << cpu);
  writel(pwr_reg, IO_ADDRESS(SW_PA_CPUCFG_IO_BASE) + AW_CPUCFG_DBGCTL1);

  /* step3: set up power-off signal */
  pwr_reg = readl(IO_ADDRESS(SW_PA_CPUCFG_IO_BASE) + AW_CPU1_PWROFF_REG);
  pwr_reg |= 1;
  writel(pwr_reg, IO_ADDRESS(SW_PA_CPUCFG_IO_BASE) + AW_CPU1_PWROFF_REG);
}
//#else


typedef unsigned int u32;



int sunxi_gpio_set_cfgpin(u32 pin, u32 val)
{
    u32 cfg;
    u32 bank = GPIO_BANK(pin);
    u32 index = GPIO_CFG_INDEX(pin);
    u32 offset = GPIO_CFG_OFFSET(pin);
    struct sunxi_gpio *pio =
        &((struct sunxi_gpio_reg *)SUNXI_PIO_BASE)->gpio_bank[bank];
    cfg = readl(&pio->cfg[0] + index);
    cfg &= ~(0xf << offset);
    cfg |= val << offset;
    writel(cfg, &pio->cfg[0] + index);
    return 0;
}

int sunxi_gpio_set_pull(u32 pin, u32 val)
{
    u32 cfg;
    u32 bank = GPIO_BANK(pin);
    u32 index = GPIO_PULL_INDEX(pin);
    u32 offset = GPIO_PULL_OFFSET(pin);
    struct sunxi_gpio *pio =
        &((struct sunxi_gpio_reg *)SUNXI_PIO_BASE)->gpio_bank[bank];
    cfg = readl(&pio->pull[0] + index);
    cfg &= ~(0x3 << offset);
    cfg |= val << offset;
    writel(cfg, &pio->pull[0] + index);
    return 0;
}

int sunxi_gpio_output(u32 pin, u32 val)
{
    u32 dat;
    u32 bank = GPIO_BANK(pin);
    u32 num = GPIO_NUM(pin);
    struct sunxi_gpio *pio =
        &((struct sunxi_gpio_reg *)SUNXI_PIO_BASE)->gpio_bank[bank];
    dat = readl(&pio->dat);
    if(val)
        dat |= 1 << num;
    else
        dat &= ~(1 << num);
    writel(dat, &pio->dat);
    return 0;
}

int sunxi_gpio_input(u32 pin)
{
    u32 dat;
    u32 bank = GPIO_BANK(pin);
    u32 num = GPIO_NUM(pin);
    struct sunxi_gpio *pio =
        &((struct sunxi_gpio_reg *)SUNXI_PIO_BASE)->gpio_bank[bank];
    dat = readl(&pio->dat);
    dat >>= num;
    return (dat & 0x1);
}

int gpio_direction_input(unsigned gpio)
{
    sunxi_gpio_set_cfgpin(gpio, SUNXI_GPIO_INPUT);
    return sunxi_gpio_input(gpio);
}

int gpio_direction_output(unsigned gpio, int value)
{
    sunxi_gpio_set_cfgpin(gpio, SUNXI_GPIO_OUTPUT);
    return sunxi_gpio_output(gpio, value);
}


